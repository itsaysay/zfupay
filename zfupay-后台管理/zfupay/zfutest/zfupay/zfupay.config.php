﻿<?php
/* *
 * 配置文件
 */
 
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//合作身份者id，以508开头的16位纯数字

require_once 'conn.php';
require_once 'lib/func.php';
$payinfo=getpayinfo();
$zfupay_config['partner']		= $payinfo['partner'];

//安全检验码，以数字和字母组成的32位字符
$zfupay_config['key']			= $payinfo['key'];

//卖家支付宝账户
$zfupay_config['seller_email']	= $payinfo['seller_email'];
//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


//签名方式 不需修改
$zfupay_config['sign_type']    = strtoupper('MD5');

//字符编码格式 目前支持 gbk 或 utf-8
$zfupay_config['input_charset']= strtolower('utf-8');

//消息验证方式 ，不需修改
$zfupay_config['transport']= strtolower('http');


?>