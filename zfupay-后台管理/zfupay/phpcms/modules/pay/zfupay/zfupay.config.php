<?php
/* *
 * 配置文件
 */

require_once("lib/zfupay_submit_class.php");
require_once("lib/zfupay_notify.class.php");

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//合作身份者id，以508开头的16位纯数字
$this->handle = pc_base::load_app_class('pay_deposit');
$payment = $this->handle->get_payment('1');
$cfg = unserialize_config($payment['config']);

$zfupay_config['partner']		= $cfg['alipay_partner'];

//安全检验码，以数字和字母组成的32位字符
$zfupay_config['key']			= $cfg['alipay_key'];


//卖家支付宝帐号
$zfupay_config['seller_email']	= $cfg['alipay_account'];
//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


//签名方式 不需修改
$zfupay_config['sign_type']    = strtoupper('MD5');

//字符编码格式 目前支持 gbk 或 utf-8
$zfupay_config['input_charset']= strtolower('utf-8');

//消息验证方式 ，不需修改
$zfupay_config['transport']= strtolower('http');


?>