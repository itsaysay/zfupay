<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('foreground','member');
pc_base::load_sys_class('format', '', 0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_app_func('global');

class zfupay extends foreground {

	public function init() {
		require_once("zfupay/zfupay.config.php");
		/*这里写自己的一些操作*/
		$title=$_POST["title"];
		$payAmount=$_POST["payAmount"];
		
		
		
		//卖家支付宝帐户
		//必填
		$seller_email = $zfupay_config['seller_email'];
		
		//商户订单号
		//商户网站订单系统中唯一订单号，必填
		$out_trade_no = $title;
		
		//付款金额
		$price = $payAmount;
		
		
		//构造要请求的参数数组，无需改动
		$parameter = array(
				"seller_email"	=> $seller_email,
				"partner"	=> trim(strtolower($zfupay_config['partner'])),
				"out_trade_no"	=> $out_trade_no,
				"price"	=> $price
		);
		
		//建立请求
		$zfupaySubmit = new zfupaySubmit($zfupay_config);

		$zfupaySubmit->buildRequestHttp($parameter);
		//logResult($seller_email.trim(strtolower($zfupay_config['partner'])).$out_trade_no.$price);
		include template('pay', 'zfupay');
	}
	
}

?>