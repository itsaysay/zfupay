<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<?php echo IMG_PATH;?>zfupay/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo IMG_PATH;?>zfupaycss/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo JS_PATH;?>jquery.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>formvalidator.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>formvalidatorregex.js" charset="UTF-8"></script>

</head>

<body>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">在线充值</a></li>
    </ul>
    </div>
  <script>
	function pdata(){
		var title=$("#title").val();
		var payAmount=$("#payAmount").val();
		$.post(
				   "<?php echo APP_PATH;?>index.php?m=pay&c=zfupay",
			  {
				title:title,
				payAmount:payAmount
			  },
			  function(data,status){
					//alert(data);
			  });
			}
  </script>
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
   
    
  	<div id="tab1" class="tabson">
    
    
<div class="col-1 " >
<h5 class="title" style="font-size:16px;">支付确认</h5>
<div class="content">
<table width="100%" cellspacing="0" class="table-list nHover">
<tr>
<td  width="120">费用：</td> 
<td><?php echo $factory_info['money'];?>元</td>
</tr>
<tr height="10px"><td></td><td></td></tr>

<?php if($logistics_fee) { ?>
<tr>
<td  width="120">配送费用：</td> 
<td><?php echo $logistics_fee;?> 元</td>
</tr>
<?php } ?>
<tr height="10px"><td></td><td></td></tr>

<?php if($discount>0) { ?>
<tr>
<td  width="120">折扣/涨价：</td> 
<td><?php echo $discount;?> 元</td>
</tr>
<?php } ?>
<tr height="10px"><td></td><td></td></tr>

<?php if($pay_fee) { ?>
<tr>
<td  width="120">手续费：</td> 
<td><?php echo $pay_fee;?>元</td>
</tr>
<?php } ?>
<tr height="10px"><td></td><td></td></tr>

<tr>
<td  width="120">合计：</td> 
<td><font style="color:#F00; font-size:22px;font-family:Georgia,Arial; font-weight:700"><?php echo $factory_info['price'];?></font>  元</td>
</tr>
<tr height="10px"><td></td><td></td></tr>

<tr>
<td  width="120">支付方式：</td> 
<td><?php echo $factory_info['payment'];?></td>
</tr>
<tr height="10px"><td></td><td></td></tr>
</table>
<div class="bk10"></div>
<?php echo $code;?>
</div>
</div>
    </div>     
	</div> 

    
    </div>


</body>

</html>