<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<?php echo IMG_PATH;?>zfupay/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo IMG_PATH;?>zfupaycss/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo JS_PATH;?>jquery.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>formvalidator.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>formvalidatorregex.js" charset="UTF-8"></script>

</head>

<body>
<script type="text/javascript">
<!--
$(function(){
	$.formValidator.initConfig({autotip:true,formid:"myform",onerror:function(msg){}});
	$("#price").formValidator({onshow:"请输入要充值的金额",onfocus:"充值金额不能为空"}).inputValidator({min:1,max:999,onerror:"充值金额不能为空"}).regexValidator({regexp:"^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){1,2})?$",onerror:"充值金额必须为整数或小数(保留两位小数)"});
	$("#contactname").formValidator({onshow:"请输入姓名",onfocus:"姓名不能为空"}).inputValidator({min:1,max:999,onerror:"姓名不能为空"});
	$("#email").formValidator({onshow:"请输入email",oncorrect:"格式正确"}).regexValidator({regexp:"email",datatype:"enum",onerror:"错误的emai格式"});	
	$("#code").formValidator({onshow:"请输入验证码",onfocus:"验证码不能为空"}).inputValidator({min:1,max:999,onerror:"验证码不能为空"}).ajaxValidator({
	    type : "get",
		url : "",
		data :"m=pay&c=deposit&a=public_checkcode",
		datatype : "html",
		async:'false',
		success : function(data){	
            if(data == 1)
			{
                return true;
			}
            else
			{
                return false;
			}
		},
		buttons: $("#dosubmit"),
		onerror : "验证码错误",
		onwait : "验证中"
	});
})
//-->
</script>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">在线充值</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
   
    
  	<div id="tab1" class="tabson">
    
    
    <div class="col-1">

<div class="content">
<?php if(isset($_GET['exchange']) && $_GET['exchange']=='point') { ?>
<div class="point" id='exchange'>
        	<a href="javascript:hide_element('exchange');" hidefocus="true" class="close"><span>关闭</span></a>
            <div class="content"><BR><p>1、您可以通过充值人民币，然后进行积分兑换的方式获取积分</p>
			<p>2、您可以通过回复评论来获取积分</p></div>
            <span class="o1"></span><span class="o2"></span><span class="o3"></span><span class="o4"></span>
        </div>
<?php } ?>
<form name="myform" action="<?php echo APP_PATH;?>index.php?m=pay&c=deposit&a=pay_recharge" method="post" id="myform">
<table width="100%" cellspacing="0" class="table_form">
    <tr>
       <td width="80">余额：</td>        
       <td style="padding:0 0 0 10px"><font style="color:#F00; font-size:22px;font-family:Georgia,Arial; font-weight:700"><?php echo $memberinfo['amount'];?></font> 元</td>
    </tr>
    <tr height="10px"><td></td><td></td></tr>
     <tr>
	<td>充值金额：</td>
	<td><input name="info[price]" type="text" id="price" size="8" value="<?php if(is_numeric($_GET['price'])) { ?><?php echo $_GET['price'];?><?php } ?>" class="dfinput">&nbsp;元<span id="msgid"></span></td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
  <tr>
       <td>充值方式：</td>
       <td>     
		<?php if($pay_types) { ?><?php echo mk_pay_btn($pay_types);?><?php } else { ?>本站暂未开启在线支付功能，如需帮助请联系管理员。<?php } ?>
	   </td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
  <tr>		
    <td>E-mail：</td>
       <td><input name="info[email]" type="text" id="email" size="30" value="<?php echo $memberinfo['email'];?>"  class="dfinput"/></td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
     <tr>
       <td>姓 名：</td>
       <td><input name="info[name]" type="text" id="contactname" size="30" value="<?php echo $memberinfo['username'];?>"  class="dfinput"/></td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
     <tr>
       <td>电 话：</td>
       <td><input name="info[telephone]" type="text" id="telephone" size="30"  class="dfinput"/> 格式：010-88888888或13888888888<span id="msgid1" ></span></td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
     <tr>
       <td>备  注：</td>
       <td><textarea name="info[usernote]"  id="usernote" rows="5" cols="46" value="" style="border: #CCC solid 1px;"></textarea></td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
     <tr>
       <td>验证码：</td>
       <td><input name="code" type="text" id="code" size="10"  class="dfinput"/> <?php echo form::checkcode('code_img','4','14',110,30);?></td>
     </tr>
     <tr height="10px"><td></td><td></td></tr>
     <tr>
       <td></td>
       <td colspan="2"><label>
         <input type="submit" name="dosubmit" id="dosubmit" value="确 定" class="btn"/>
         </label></td>
     </tr>
   </table>
   </form>
   </div>
   </div>
    </div>     
	</div> 

    
    </div>


</body>

</html>
