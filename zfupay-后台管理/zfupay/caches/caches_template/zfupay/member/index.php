<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<?php echo IMG_PATH;?>zfupay/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo IMG_PATH;?>zfupay/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo IMG_PATH;?>zfupay/js/jsapi.js"></script>
<script type="text/javascript" src="<?php echo IMG_PATH;?>zfupay/js/format+zh_CN,default,corechart.I.js"></script>		
<script type="text/javascript" src="<?php echo IMG_PATH;?>zfupay/js/jquery.gvChart-1.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo IMG_PATH;?>zfupay/js/jquery.ba-resize.min.js"></script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="<?php echo APP_PATH;?>index.php?m=member&c=index&a=main" target="_parent">首页</a></li>
    <li><a href="#">工作台</a></li>
    </ul>
    </div>
    
    
    <div class="mainbox">
    
    <div class="mainleft">
    
    
    <div class="leftinfo">
    <div class="listtitle"><a href="#" class="more1">更多</a>数据统计</div>
        
    <div class="maintj">  
    <table id='myTable5'>
				<caption>流量比例：您这个月有免费流量10000元，已用125元，剩余9875元</caption>
				<thead>
					<tr>
						<th></th>
						
						<th>已用流量</th>
						
						<th>剩余流量</th>
						
					</tr>
				</thead>
					<tbody>
					<tr>
						
						<td>125</td>
						
						<td>9875</td>
						
					</tr>
				</tbody>
			</table>  
    </div>
    
    </div>
    <!--leftinfo end-->
    
    
    <div class="leftinfos">
    
   
    <div class="infoleft">
    
    <div class="listtitle"><a href="#" class="more1">更多</a>站长新闻</div>    
    <ul class="newlist">
    <li><a href="#">上海自贸区今日正式挂牌成立</a><b>10-09</b></li>
    <li><a href="#">习近平将访东南亚两国 首次出席APEC峰会</a><b>10-08</b></li>
    <li><a href="#">最高法:谎称炸弹致航班备降者从重追刑责</a><b>10-09</b></li>
    <li><a href="#">华北大部遭遇雾霾天 北京全城陷重污染</a><b>10-06</b></li>
    <li><a href="#">"环保专家"董良杰涉嫌寻衅滋事被刑拘</a><b>10-05</b></li>
    <li><a href="#">中央巡视组：重庆对一把手监督不到位</a><b>10-04</b></li>
    <li><a href="#">囧!悍马没改好成华丽马车(图)</a><b>10-03</b></li>
    </ul>   
    
    </div>
    
    
    <div class="inforight">
    <div class="listtitle"><a href="#" class="more1">添加</a>常用工具</div>
    
    <ul class="tooli">
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d01.png" /></span><p><a href="#">信息资料</a></p></li>
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d02.png" /></span><p><a href="#">编辑</a></p></li>
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d03.png" /></span><p><a href="#">记事本</a></p></li>
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d04.png" /></span><p><a href="#">任务日历</a></p></li>
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d05.png" /></span><p><a href="#">图片管理</a></p></li>
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d06.png" /></span><p><a href="#">交易</a></p></li>
    <li><span><img src="<?php echo IMG_PATH;?>zfupay/images/d07.png" /></span><p><a href="#">档案袋</a></p></li>    
    </ul>
    
    </div>
    
    
    </div>
    
    
    </div>
    <!--mainleft end-->
    
    
    <div class="mainright">
    
    
    <div class="dflist">
    <div class="listtitle"><a href="#" class="more1">更多</a>系统公告</div>    
    <ul class="newlist">
    <li><a href="#">上海自贸区今日正式挂牌成立</a></li>
    <li><a href="#">习近平将访东南亚两国 首次出席APEC峰会</a></li>
    <li><a href="#">最高法:谎称炸弹致航班备降者从重追刑责</a></li>
    <li><a href="#">华北大部遭遇雾霾天 北京全城陷重污染</a></li>
    <li><a href="#">"环保专家"董良杰涉嫌寻衅滋事被刑拘</a></li>
    <li><a href="#">中央巡视组：重庆对一把手监督不到位</a></li>
    <li><a href="#">囧!悍马没改好成华丽马车(图)</a></li>
    <li><a href="#">澳门黄金周加派稽查人员监察出租车违规行为</a></li>
    <li><a href="#">香港环境局长吁民众支持政府扩建堆填区</a></li> 
    </ul>        
    </div>
    
    
    <div class="dflist1">
    <div class="listtitle">天气</div>    
    <iframe allowtransparency="true" frameborder="0" width="288" height="238" scrolling="no" src="http://tianqi.2345.com/plugin/widget/index.htm?s=2&z=1&t=1&v=1&d=2&bd=0&k=000000&f=000000&q=1&e=1&a=1&c=54511&w=288&h=238&align=center"></iframe>       
    </div>
    
    

    
    
    </div>
    <!--mainright end-->
    
    
    </div>



</body>
<script type="text/javascript">
	setWidth();
	$(window).resize(function(){
		setWidth();	
	});
	function setWidth(){
		var width = ($('.leftinfos').width()-12)/2;
		$('.infoleft,.inforight').width(width);
	}
</script>
</html>
