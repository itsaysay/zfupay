<?php defined('IN_PHPCMS') or exit('No permission resources.'); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<?php echo IMG_PATH;?>zfupay/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo IMG_PATH;?>zfupaycss/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo JS_PATH;?>jquery.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>formvalidator.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>formvalidatorregex.js" charset="UTF-8"></script>

</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">帐号设置</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
   
    
  	<div id="tab1" class="tabson">
    
    
    <form method="post" action="" id="myform" name="myform">
    <ul class="forminfo">
    <li><label><?php echo L('email');?><b>*</b></label><input name="info[email]" type="text"  id="email" class="dfinput" value="<?php echo $memberinfo['email'];?>"  style="width:300px;"/></li>
    <li><label><?php echo L('old_password');?><b>*</b></label><input name="info[password]" type="password" id="password" class="dfinput" value=""  style="width:300px;"/></li>
	<li><label><?php echo L('new_password');?><b>*</b></label><input name="info[newpassword]" type="password" id="newpassword" class="dfinput" value=""  style="width:300px;"/></li>
    <li><label><?php echo L('re_input').L('new_password');?><b>*</b></label><input name="info[renewpassword]" id="renewpassword"  type="password" class="dfinput" value=""  style="width:300px;"/></li>
    <li><label>&nbsp;</label><input name="dosubmit" type="submit"  id="dosubmit" class="btn" value="<?php echo L('submit');?>"/></li>
    </ul>
    </form>
    </div>     
	</div> 
    <script type="text/javascript">
<!--
$(function(){
	$.formValidator.initConfig({autotip:true,formid:"myform",onerror:function(msg){}});
	$("#password").formValidator({onshow:"<?php echo L('input').L('password');?>",onfocus:"<?php echo L('password').L('between_6_to_20');?>"}).inputValidator({min:6,max:20,onerror:"<?php echo L('password').L('between_6_to_20');?>"});
	$("#newpassword").formValidator({onshow:"<?php echo L('input').L('password');?>",onfocus:"<?php echo L('password').L('between_6_to_20');?>"}).inputValidator({min:6,max:20,onerror:"<?php echo L('password').L('between_6_to_20');?>"});
	$("#renewpassword").formValidator({onshow:"<?php echo L('input').L('cofirmpwd');?>",onfocus:"<?php echo L('input').L('passwords_not_match');?>",oncorrect:"<?php echo L('passwords_match');?>"}).compareValidator({desid:"newpassword",operateor:"=",onerror:"<?php echo L('input').L('passwords_not_match');?>"});	
	$("#email").formValidator({onshow:"<?php echo L('input').L('email');?>",onfocus:"<?php echo L('email').L('format_incorrect');?>",oncorrect:"<?php echo L('email').L('format_right');?>"}).inputValidator({min:2,max:32,onerror:"<?php echo L('email').L('between_2_to_32');?>"}).regexValidator({regexp:"email",datatype:"enum",onerror:"<?php echo L('email').L('format_incorrect');?>"}).ajaxValidator({
	    type : "get",
		url : "",
		data :"m=member&c=index&a=public_checkemail_ajax",
		datatype : "html",
		async:'false',
		success : function(data){	
            if( data == "1" ) {
                return true;
			} else {
                return false;
			}
		},
		buttons: $("#dosubmit"),
		onerror : "<?php echo L('deny_register').L('or').L('email_already_exist');?>",
		onwait : "<?php echo L('connecting_please_wait');?>"
	}).defaultPassed();
})
//-->
</script>
    
    
    
    </div>


</body>

</html>
