<?php
return array (
  1 => 
  array (
    'siteid' => '1',
    'name' => '直付宝',
    'dirname' => '',
    'domain' => 'http://www.178le.com/',
    'site_title' => '欢迎使用支付宝、财付通(筹)即时到账服务，安全，零门槛，无需签约',
    'keywords' => '欢迎使用支付宝、财付通(筹)即时到账服务，安全，零门槛，无需签约，不试试怎么知道好不好用',
    'description' => '欢迎使用支付宝、财付通(筹)即时到账服务，安全，零门槛，无需签约，不试试怎么知道好不好用',
    'release_point' => '',
    'default_style' => 'zfupay',
    'template' => 'zfupay',
    'setting' => 'array (
  \'upload_maxsize\' => \'2048\',
  \'upload_allowext\' => \'jpg|jpeg|gif|bmp|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|rar|zip|swf\',
  \'watermark_enable\' => \'1\',
  \'watermark_minwidth\' => \'300\',
  \'watermark_minheight\' => \'300\',
  \'watermark_img\' => \'statics/images/water//mark.png\',
  \'watermark_pct\' => \'85\',
  \'watermark_quality\' => \'80\',
  \'watermark_pos\' => \'9\',
)',
    'uuid' => '5849e543-9bf3-11e4-a3bb-7d5f61cbd7d4',
    'url' => 'http://www.178le.com/',
  ),
);
?>