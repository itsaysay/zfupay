<?php
return array (
  'allowregister' => '1',
  'choosemodel' => '0',
  'enablemailcheck' => '0',
  'enablcodecheck' => '0',
  'mobile_checktype' => '0',
  'user_sendsms_title' => '',
  'registerverify' => '0',
  'showapppoint' => '0',
  'rmb_point_rate' => '10',
  'defualtpoint' => '0',
  'defualtamount' => '0',
  'showregprotocol' => '0',
  'regprotocol' => '',
  'registerverifymessage' => ' 欢迎您注册成为直付宝用户，您的账号需要邮箱认证，点击下面链接进行认证：{click}
或者将网址复制到浏览器：{url}',
  'forgetpassword' => ' phpcms密码找回，请在一小时内点击下面链接进行操作：{click}
或者将网址复制到浏览器：{url}',
);
?>