﻿<?php
/* *
 * 功能：服务器异步通知页面

 *************************页面功能说明*************************
 * 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
 * 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
 * 该页面调试工具请使用写文本函数logResult
 * 如果没有收到该页面返回的 success 信息，服务器会在24小时内按一定的时间策略重发通知
 */
require_once("zfupay.config.php");

//计算得出通知验证结果
$zfupayNotify = new ZfupayNotify($zfupay_config);
$verify_result = $zfupayNotify->verifyNotify();
logResult($verify_result);
if(true) {//验证成功
	
	//请在这里加上商户的业务逻辑程序代
	//商户订单号
	$out_trade_no = @$_POST['out_trade_no'];

	//交易状态
	$trade_status = @$_POST['states'];

	//交易金额
	$trade_amount = @$_POST['price'];

	if($trade_status=='Y') {
	//请根据交易状态，交易金额，订单号判断是否跟自己系统的一致
		
		updatestates($out_trade_no);
		//setbalance($trade_amount);
         echo "ok";		//请不要修改或删除
		//logResult(setbalance($trade_amount));
    }else{
    	echo "fail";	
    }
	
}
else {
    //验证失败
    echo "fail";
}
?>