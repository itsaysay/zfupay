﻿# Host: localhost  (Version: 5.5.47)
# Date: 2018-02-02 17:57:17
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "account"
#

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `callback` varchar(255) DEFAULT NULL,
  `partner` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "account"
#

/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'zfupay@126.com','123456','zfupay@126.com','028FC021E42B9D2B6FCE5DC43868BF2F','http://127.0.0.1:8080/phpstudy/zfupay/zfupay/notify_url.php','5081234567890000');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;

#
# Structure for table "client2serverdata"
#

DROP TABLE IF EXISTS `client2serverdata`;
CREATE TABLE `client2serverdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trsno` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `partner` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `sign` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

#
# Data for table "client2serverdata"
#

/*!40000 ALTER TABLE `client2serverdata` DISABLE KEYS */;
INSERT INTO `client2serverdata` VALUES (18,'20150110193508','zfupay@126.com','5081234567890000','0.01','6363b1325e1c39fc8a6c68e89bb2d595'),(19,'20150110193554','zfupay@126.com','5081234567890000','0.01','76b6af7b7c2e4033958cd32b4f297931'),(20,'20150111000546','zfupay@126.com','5081234567890000','0.01','c04c4423fbdc92314929f88b0664beb7'),(21,'20150111005427','zfupay@126.com','5081234567890000','0.01','94ec07437b27a4f4d9a09b898cb0b2be');
/*!40000 ALTER TABLE `client2serverdata` ENABLE KEYS */;

#
# Structure for table "orderlist"
#

DROP TABLE IF EXISTS `orderlist`;
CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `times` varchar(255) DEFAULT NULL,
  `trsno` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `states` varchar(255) DEFAULT NULL,
  `isout` varchar(255) DEFAULT 'N',
  `callbackstates` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#
# Data for table "orderlist"
#

/*!40000 ALTER TABLE `orderlist` DISABLE KEYS */;
INSERT INTO `orderlist` VALUES (1,'2015/1/7 10:49:00','201501071033','0.01','Y','Y','ok'),(2,'2015/1/5 2:32:00','宝-转出到余额','622.80','Y','Y',''),(3,'2015/1/4 13:45:00','','130.00','Y','Y',''),(4,'2014/12/24 22:40:00','快乐，自己买个苹果吃[包养你]','5.20','Y','Y',''),(5,'2014/12/22 17:06:00','码-转账','498.00','Y','Y',''),(6,'2014/12/11 11:32:00','201501071033','1000.00','Y','N',''),(7,'2015/1/10 17:45:00','20150110171410','0.01','Y','N',NULL),(8,'2015/1/10 17:35:00','20150110171015','0.01','Y','N',NULL),(9,'2014/12/11 11:32:00','域名系统','1000.00','Y','N',NULL),(10,'2015/1/10 20:00:00','20150110193554','0.01','Y','Y','ok');
/*!40000 ALTER TABLE `orderlist` ENABLE KEYS */;
